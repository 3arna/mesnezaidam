<?php


?>
<link rel="stylesheet" type="text/css" href="../../functions/clock/clock.css"/>
<script type="text/javascript">

    
    function whiteblack(n){
        
            n = parseInt(n);
            var rez;
            if(n%2){
                rez = 'whiteclock';    
            }
            else
                rez = 'blackclock';
            
            
            return rez;
            
     }
    
    function clock(){
        
        var targettime = "11/4/2010 12:00 AM";
        
        
        var thetime = new Date();
        
        var thedays = thetime.getDay();
        var thehours = thetime.getHours();
        var theminutes = thetime.getMinutes();
        var theseconds = thetime.getSeconds();
        
        document.getElementById("seconds").setAttribute("class", ""+whiteblack(theseconds)+"");
        document.getElementById("minutes").setAttribute("class", ""+whiteblack(theminutes)+"");
        document.getElementById("hours").setAttribute("class", ""+whiteblack(thehours)+"");
        document.getElementById("days").setAttribute("class", ""+whiteblack(thedays)+"");
        
        document.getElementById("thedays").innerHTML = thedays;
        document.getElementById("thehours").innerHTML = thehours;
        document.getElementById("theminutes").innerHTML = theminutes;
        document.getElementById("theseconds").innerHTML = theseconds;
        
        setTimeout('clock()',1000);
        
    }
    clock();

</script>


    
        <table id="clock" cellpadding="0" cellspacing="1">
            <tr>
                <td id="days">
                    <div class="clocktitle">
                        days
                    </div>
                    <div class="clocknumbers" id="thedays">
                    </div>
                </td>
                <td id="hours">
                    <div class="clocktitle">
                        hours
                    </div>
                    <div class="clocknumbers" id="thehours">
                    </div>
                </td>
                <td id="minutes">
                    <div class="clocktitle">
                        minutes
                    </div>
                    <div class="clocknumbers" id="theminutes">
                    </div>
                </td>
                <td id="seconds">
                    <div class="clocktitle">
                        seconds
                    </div>
                    <div class="clocknumbers" id="theseconds">
                    </div>
                </td>
            </tr>
        </table>