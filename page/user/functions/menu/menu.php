<link type="text/css" rel="stylesheet" href="../functions/menu/menu.css" />



<?php

$curentpage = $_SESSION['acclasttype'];

if($curentpage=='pointer'){
    $curentcategory = $_SESSION['Pcategory'];
    $curentsubcategory = $_SESSION['Psubcategory'];
}
if($curentpage=='framer'){
    $curentcategory = $_SESSION['Fcategory'];
    $curentsubcategory = $_SESSION['Fsubcategory'];
}

//self menu categories
$curentdir = getcwd();
$self = $curentdir.'/menu/categories';
$self = scandir("$self");

$selfcount = count($self);

$menu = array();

for($i=2; $i<$selfcount; $i++){
                        
    $menu[] = $self[$i];
}


//global menu categories

$dir = $curentdir;
$globallenght = strlen($curentdir);
$slashpos = strrpos($curentdir,"\\");

for($i=$slashpos; $i<$globallenght; $i++){
    
    $dir = substr_replace($dir, "", "$slashpos");
}

$dir = $dir.'/functions/menu/categories';
$dir = scandir("$dir");

$globalcount = count($dir);

for($i=2; $i<$globalcount; $i++){
    
    $menu[] = $dir[$i];
}

$categoriescount = count($menu);

?>

<table id="mane" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="5" height="1" bgcolor="black">
        </td>
    </tr>
    <tr>
        <td width="200" height="300" align="right">
            <ul id="menu" style="padding-right: 5px;">

                <?php
                    for($i=0; $i<$categoriescount; $i++){
                        $value = $menu[$i];
                        ?>
                        
                        <li class="categories black" onclick="activecategory('<?php echo($menu[$i]);?>','<?php echo($curentpage);?>')" id="<?php echo($menu[$i]);?>"><?php echo($lang[$value]);?></li>
                        
                        <?php
                        
                    }

                ?>
    
            </ul>
        </td>
        <td width="1" bgcolor="black">
        </td>
        <td id="container" width="500">
        </td>
        <td width="1" bgcolor="black">
        </td>
        <td valign="top" width="100">
            MENU
        </td>
    </tr>
</table>


<script type="text/javascript">
    
    
    
    function activecategory(category,curentpage,loading){
        
        $(".categories").removeClass('color').addClass('black');
        $("#"+category+"").addClass('color').removeClass('black');
        $(".loaded").removeClass('loaded');
        
        $.ajax({
            type: 'POST',
            url: '../functions/menu/clicks.php?category='+category+'&curentpage='+curentpage+'&loading='+loading,
            success: function(result){
                
                $("#container").html(result);
            }
        });
        
    }

    function activesubcategory(subcategory,curentpage){
        
        $(".submenu").removeClass('activesub').addClass('notactivesub');
        $("#"+subcategory+"").addClass('activesub').removeClass('notactivesub');
        $(".subcontainer").addClass('subcontainernotactive').removeClass('subcontaineractive');
        $("."+subcategory+" div:first-child").addClass('subcontaineractive').removeClass('subcontainernotactive');
        
        var category = $(".categories.color").attr("id");
        
        
        if(!$(".subcontaineractive").hasClass('loaded')){
        
            $.ajax({
                type: 'POST',
                url: '../functions/menu/sub.php?subcategory='+subcategory+'&curentpage='+curentpage+'&category='+category+'&loaded=no',
                success: function(result){
                    $(".subcontaineractive").html(result);
                }
            });
        
        }
        
        if($(".subcontaineractive").hasClass('loaded')){
        
            $.ajax({
                type: 'POST',
                url: '../functions/menu/sub.php?subcategory='+subcategory+'&curentpage='+curentpage+'&category='+category+'&loaded=yes'
            });
        
        }
        
        $("."+subcategory+" div:first-child").addClass('loaded');
        
        var contwidth = $("#container").width();
        var subwidth = $(".submenu:first").width();
        var subcount = $(".submenu").size();
        
        $(".submenu:last").css("margin","0");
        
        var curentwidth = contwidth +1 -(subcount*(subwidth+1));
        
        var animation = 'on';
        
        if(animation == 'on'){
            $(".subcontaineractive").animate({"width": ""+curentwidth+"px"}, "slow");
            $(".subcontainernotactive").animate({"width": "-"+curentwidth+"px"}, "slow");
        }
        
        if(animation == 'off'){
            $(".subcontaineractive").width(curentwidth);
            $(".subcontainernotactive").width(0);
        }
    }
    
    $(document).ready(function(){
        
       activecategory('<?php echo($curentcategory."','".$curentpage) ?>','yes');
       
    });
    
    
</script>
